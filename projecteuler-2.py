def fib(n : int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def fib_seq():
  return sum(list(fib(i) for i in range(50000000) if fib(i) % 2 == 0 and fib(i) < 40000000))

print(fib_seq())
