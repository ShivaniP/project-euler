def pythagoreas_triplet():
  return list(a * b * c for a in range(100) for b in range(100) for c in range(100) if a * a + b * b == c * c and a + b + c == 100)


