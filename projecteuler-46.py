def is_prime(n:int) -> bool:
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r = r +2
  return True

def prime_num(num):
  return list(i for i in range(num) if is_prime(i))

def odd_composite(num):
  return list(i for i in range(num) if i % 2 != 0 and is_prime(i) != True )

def conjecture(num):
  return min(set(i for i in odd_composite(num) for j in prime_num(num) for k in range(num) if j + 2 * pow(k,2) != i))

print(conjecture(1000000))
