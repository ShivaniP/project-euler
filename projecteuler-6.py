def square(n:int) -> int:
  return n * n

def sum_of_num(num):
  return sum(list(square(i) for i in range(1,num+1)))

def indi_sum(num):
  return pow(sum(list(range(num + 1))),2)

def diff(num):
  return sum_of_num(num) - indi_sum(num)

