def is_prime(n:int) -> bool:
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r = r +2
  return True

def factors(num):
  return max(list(i for i in range(1,num) if num % i == 0 and is_prime(i)))

